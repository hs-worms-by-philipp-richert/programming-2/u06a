#include "cSchachfeld.h"

// Constructor
cSchachfeld::cSchachfeld(char x_in, int y_in, cSchachfeld * prev_in) {
	coordX = char(std::tolower(x_in));
	coordY = y_in;
	prev = prev_in;
}

// Deconstructor
cSchachfeld::~cSchachfeld() {
	if (prev != NIL) {
		delete prev;
	}
}

// Coords Getter
tuple<char, int> cSchachfeld::getCoords() {
	return tuple<char, int>(coordX, coordY);
}

// Anhaengen eines Zugs an die verkettete Liste
cSchachfeld* cSchachfeld::laeuferZug() {
	char tmpX;
	int tmpY;
	bool moveValid = false;

	while (!moveValid) {	// Wiederholen der Eingabe bis der Zug valide ist
		askNextMove(tmpX, tmpY);

		moveValid = cSchachfeld::movedCorrectly(tmpX, tmpY);	// Zug pruefen
		if (!moveValid) {
			cout << "Unguelitger Zug fuer den Laeufer" << endl;
			continue;
		}

		moveValid = cSchachfeld::isFieldFree(tmpX, tmpY);		// Kollision pruefen
		if (!moveValid) cout << "Feld ist belegt, erneuter Zug" << endl;
	}

	cSchachfeld* p_chess = new cSchachfeld(tmpX, tmpY, this);

	return p_chess;
}

// Hilfsmethode zum Pruefen auf einen validen Laeufer-Zug
bool cSchachfeld::movedCorrectly(char x, int y) {
	int xAsInt = coordX - 'a';	// Coords auf Werte zwischen 0-7 normieren
	int yNormed = coordY - 1;	// (einfacher zum rechnen)
	int newXAsInt = x - 'a';
	int newYNormed = y - 1;

	bool validMove = false;
	int n1 = abs(newXAsInt - xAsInt);
	int n2 = abs(newYNormed - yNormed);

	validMove = ((n1 == n2) && (n2 > 0));	// |x2-x1| = |y2-y1| > 0 => valid bishop move

	return validMove;
}

// Hilfsethode zum Pruefen auf Kollisionen
bool cSchachfeld::isFieldFree(char x, int y) {
	bool failed = false;
	listFigures activeFigures = {
		tuple<char, int>('g', 1),
		tuple<char, int>('a', 2),
		tuple<char, int>('a', 3),
		tuple<char, int>('c', 6),
		tuple<char, int>('e', 6),
		tuple<char, int>('f', 6),
		tuple<char, int>('d', 8),
		tuple<char, int>('e', 8),
		tuple<char, int>('h', 8)
	};

	for (listFigures::const_iterator i = activeFigures.begin(); i != activeFigures.end(); i++) {
		if (x == std::get<char>(*i) && y == std::get<int>(*i)) failed = true;
	}

	return !failed;
}

// Hilfsmethode fuer die Tastatureingabe des naechsten Zugs
void cSchachfeld::askNextMove(char& x, int& y) {
	string input;
	char tmpX;
	int tmpY;
	bool done = false;

	while (!done) {
		cout << endl << "Geben Sie Ihren naechsten Zug fuer den Laufer ein (A1-H8): ";
		cin >> input;

		tmpX = char(std::tolower(input[0]));
		tmpY = input[1] - '0';

		if ((tmpX >= 'a' && tmpX <= 'h') && (tmpY >= 1 && tmpY <= 8)) {
			done = true;
			x = tmpX;
			y = tmpY;
		}
		else {
			cout << "Ungueltige Eingabe, Wert muss zwischen A1 und H8 liegen." << endl;
		}
	}
}

// Rekursive Ausgabemethode
void cSchachfeld::ausgabe() {
	if (prev != NIL) {
		prev->ausgabe();
	}
	else {
		cout << endl << "Alle Zuege des Laeufers:" << endl;
	}

	cout << char(std::toupper(coordX)) << coordY << endl;
}
