#pragma once

#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include <cmath>

#define NIL (cSchachfeld *)0

using namespace std;

typedef vector<tuple<char, int>> listFigures;

class cSchachfeld
{
private:
	char coordX;
	int coordY;
	cSchachfeld* prev;
	bool movedCorrectly(char, int);
	bool isFieldFree(char, int);
	void askNextMove(char&, int&);
public:
	cSchachfeld(char, int, cSchachfeld * = NIL);
	~cSchachfeld();
	cSchachfeld* laeuferZug();
	tuple<char, int> getCoords();
	void ausgabe();
};
