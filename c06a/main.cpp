// Verkettete Listen - u06a
// Schachspiel - Laeufer
// Philipp Richert
// 24.11.2022
// 
// Ich entschuldige ich im Vorfeld dass ich das Programm an manchen Stellen
// over-engineered habe, hab ich etwas mitreissen lassen xD

#include "cSchachfeld.h"
#include <iostream>

using namespace std;

int main() {
	cSchachfeld* p_chess = new cSchachfeld('e', 7);
	tuple<char, int> king = tuple<char, int>('c', 1);	// Pos des Koenigs

	while (king != p_chess->getCoords()) {	// Wiederholen bis zum Schachmatt
		p_chess = p_chess->laeuferZug();
	}

	if (king == p_chess->getCoords()) {
		cout << endl << "Schachmatt!" << endl;
	}

	p_chess->ausgabe();

	delete p_chess;

	return 0;
}
